# Content

This directory contains Cram based tests.

## generic

Contains tests which could be used on all devices.

## turris-omnia

Contains tests which could be used against `Turris Omnia` device.
